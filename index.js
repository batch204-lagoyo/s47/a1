// Directions:
// Create two event listeners for when a user types in the first and last name inputs
// When this event triggers, update the span-full-name's content to show the value of the first name input on the left and the value of the last name input on the right

// Stretch goal: Instead of an anonymous function, create a new function that the two event listers will call

//Guide question: Where do the names come from and where should they go?

let txtFirstName = document.querySelector('#txt-first-name');
let txtLastName = document.querySelector('#txt-last-name');
let spanFullName = document.querySelector('#span-full-name');

const updateFullName = () => {
	let firstName = txtFirstName.value
	let lastName = txtLastName.value

	spanFullName.innerHTML = `${firstName} ${lastName}`
}

txtFirstName.addEventListener('keyup', () => {
	updateFullName()
})
txtLastName.addEventListener('keyup', updateFullName)
